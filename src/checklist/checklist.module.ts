import { Module } from '@nestjs/common';
import { ChecklistService } from './checklist.service';
import { ChecklistController } from './checklist.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Employee } from 'src/employees/entities/employee.entity';
import { ChecklistItem } from './entities/checklist_item.entity';
import { Checklist } from './entities/checklist.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Employee, ChecklistItem, Checklist])],
  controllers: [ChecklistController],
  providers: [ChecklistService],
})
export class ChecklistModule {}
