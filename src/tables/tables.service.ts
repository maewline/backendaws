import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateTableDto } from './dto/create-table.dto';
import { UpdateTableDto } from './dto/update-table.dto';
import { Table } from './entities/table.entity';

@Injectable()
export class TablesService {
  constructor(
    @InjectRepository(Table)
    private tablesRepository: Repository<Table>,
  ) {}
  create(createTableDto: CreateTableDto) {
    // const table: Table = new Table();
    // table.name = createTableDto.name;
    // table.amount = createTableDto.amount;
    // table.status = 'พร้อมใช้งาน';
    return this.tablesRepository.save(createTableDto);
  }

  findAll() {
    return this.tablesRepository.find({
      relations: ['orders'],
      order: { id: 'ASC' },
    });
  }

  findAllByStatus(status: string) {
    return this.tablesRepository.find({
      where: { status: status },
      relations: ['orders'],
      order: { id: 'ASC' },
    });
  }

  findOne(id: number) {
    return this.tablesRepository.findOne({
      where: { id },
      relations: ['orders'],
    });
  }

  async update(id: number, updateTableDto: UpdateTableDto) {
    const table = await this.tablesRepository.findOneBy({ id });
    if (!table) {
      throw new NotFoundException();
    }
    const updatedTable = { ...table, ...updateTableDto };

    return this.tablesRepository.save(updatedTable);
  }

  async remove(id: number) {
    const table = await this.tablesRepository.findOneBy({ id });
    if (!table) {
      throw new NotFoundException();
    }
    return this.tablesRepository.remove(table);
  }
}
