import { Injectable } from '@nestjs/common';
import { CreateShortLinkDto } from './dto/create-short-link.dto';
import { UpdateShortLinkDto } from './dto/update-short-link.dto';
import { v4 as uuid } from 'uuid';
// import { nanoid } from 'nanoid';
import { InjectRepository } from '@nestjs/typeorm';
import { ShortLink } from './entities/short-link.entity';
import { Repository } from 'typeorm';

@Injectable()
export class ShortLinkService {
  private urls = new Map<
    string,
    { longUrl: string; expireTimeout: NodeJS.Timeout }
  >();

  constructor(
    @InjectRepository(ShortLink)
    private urlRepository: Repository<ShortLink>,
  ) {}
  async create(createShortLinkDto: CreateShortLinkDto) {
    console.log('create');
    setTimeout(async () => {
      console.log('xxxxxxxxxxxxxxxxx');
    }, 5000); // กำหนดเวลาหมดอายุภายใน 5 วินาที

    const shortUrl = uuid(6);
    const urll: ShortLink = {
      expirationTime: new Date(),
      longUrl: createShortLinkDto.longUrl,
      shortUrl: shortUrl,
    };
    this.urlRepository.save(urll);

    return `${process.env.BASE_URL}/${shortUrl}`;
  }

  findAll() {
    return this.urlRepository.find();
  }

  findOne(shortUrl: string) {
    const url = this.urlRepository.findOne({ where: { shortUrl } });
    if (!url) {
      return 'ไม่ตรง: ' + shortUrl;
    }
    // clearTimeout(url.expireTimeout); // ยกเลิกการตั้งเวลาหมดอายุ
    return url;
  }

  update(id: number, updateShortLinkDto: UpdateShortLinkDto) {
    return `This action updates a #${id} shortLink`;
  }

  remove(id: number) {
    return `This action removes a #${id} shortLink`;
  }
}
