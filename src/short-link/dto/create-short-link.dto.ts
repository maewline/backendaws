export class CreateShortLinkDto {
  longUrl: 'https://www.google.com/';
  expirationTime: Date = new Date();
}
