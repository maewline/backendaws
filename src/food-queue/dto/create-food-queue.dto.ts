import { IsNotEmpty, Length } from 'class-validator';
import { OrderItem } from 'src/orders/entities/order-item';

export class CreateFoodQueueDto {
  // @IsNotEmpty()
  name: string;

  // @IsNotEmpty()
  status: string;

  // @IsNotEmpty()
  note: string;
  chefName: string;
  orderItem: OrderItem;
  waiterName: string;
}
