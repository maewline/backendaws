import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { FoodQueueService } from './food-queue.service';
import { CreateFoodQueueDto } from './dto/create-food-queue.dto';
import { UpdateFoodQueueDto } from './dto/update-food-queue.dto';

@Controller('food-queue')
export class FoodQueueController {
  constructor(private readonly foodQueueService: FoodQueueService) {}

  @Post()
  create(@Body() createFoodQueueDto: CreateFoodQueueDto) {
    return this.foodQueueService.create(createFoodQueueDto);
  }

  @Get()
  findAll() {
    return this.foodQueueService.findAll();
  }
  //เอา food-queue ทั้งหมด โดยใช้ order id
  @Get('/findFoodQueueByOrderId/:idOrder')
  findFoodQueueByOrderId(@Param('idOrder') idOrder: string) {
    return this.foodQueueService.findFoodQueueByOrderId(idOrder);
  }
  //เอา food-queue ทั้งหมด ไม่สนสถานะ FoodQueue
  @Get('/findFoodQueueNotUnPaid/:idTable')
  findFoodQueueNotUnPaid(@Param('idTable') idTable: number) {
    return this.foodQueueService.findFoodQueueNotUnPaid(idTable);
  }
  //เอา food-queue ทั้งหมด ระบุสถานะ FoodQueue = ?
  @Get('/findFoodQueueByStatus/:statusFoodQueue/:idOrder')
  findFoodQueueByStatus(
    @Param('statusFoodQueue') status: string,
    @Param('idOrder') idOrder: string,
  ) {
    return this.foodQueueService.findFoodQueueByStatus(status, idOrder);
  }

  @Get('/getStatusFoodQueue')
  findAllChef() {
    return this.foodQueueService.findAllChef();
  }

  @Get('/myFoodQueue/:chefName')
  findMyFoodQueue(@Param('chefName') chefName: string) {
    return this.foodQueueService.findMyFoodQueue(chefName);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.foodQueueService.findOne(+id);
  }

  // @Get('/findOnePendingConfirmation/:id')
  // findPendingConfirmation(@Param('id') id: string) {
  //   return this.foodQueueService.findOnePendingConfirmation(+id);
  // }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateFoodQueueDto: UpdateFoodQueueDto,
  ) {
    return this.foodQueueService.update(+id, updateFoodQueueDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.foodQueueService.remove(+id);
  }
}
