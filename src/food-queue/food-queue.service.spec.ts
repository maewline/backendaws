import { Test, TestingModule } from '@nestjs/testing';
import { FoodQueueService } from './food-queue.service';

describe('FoodQueueService', () => {
  let service: FoodQueueService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FoodQueueService],
    }).compile();

    service = module.get<FoodQueueService>(FoodQueueService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
