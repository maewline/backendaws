import { Test, TestingModule } from '@nestjs/testing';
import { FoodQueueController } from './food-queue.controller';
import { FoodQueueService } from './food-queue.service';

describe('FoodQueueController', () => {
  let controller: FoodQueueController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [FoodQueueController],
      providers: [FoodQueueService],
    }).compile();

    controller = module.get<FoodQueueController>(FoodQueueController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
