import { Module } from '@nestjs/common';
import { FoodQueueService } from './food-queue.service';
import { FoodQueueController } from './food-queue.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FoodQueue } from './entities/food-queue.entity';
import { Employee } from 'src/employees/entities/employee.entity';
import { OrderItem } from 'src/orders/entities/order-item';

@Module({
  imports: [TypeOrmModule.forFeature([FoodQueue, Employee, OrderItem])],
  controllers: [FoodQueueController],
  providers: [FoodQueueService],
})
export class FoodQueueModule {}
