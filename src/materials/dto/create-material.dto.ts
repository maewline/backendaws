import { IsNotEmpty, Length } from 'class-validator';

export class CreateMaterialDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  amount: number;

  @IsNotEmpty()
  minimum: number;

  @IsNotEmpty()
  unit: string;
}
