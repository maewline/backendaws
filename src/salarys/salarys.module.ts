import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Checkinscheckoit } from 'src/checkinscheckoits/entities/checkinscheckoit.entity';
import { Employee } from 'src/employees/entities/employee.entity';
import { Salary } from './entities/salary.entity';
import { SalarysController } from './salarys.controller';
import { SalarysService } from './salarys.service';

@Module({
  imports: [TypeOrmModule.forFeature([Salary, Checkinscheckoit, Employee])],
  controllers: [SalarysController],
  providers: [SalarysService],
})
export class SalarysModule {}
